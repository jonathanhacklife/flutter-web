# Issues

A continuación se detallan algunos errores encontrados al crear o migrar un proyecto a Flutter Web

## Failed to establish connection with the application instance in Chrome.

Recibe este error al ejecutar el proyecto en flutter web:

![](https://gitlab.com/jonathanhacklife/flutter-web/-/raw/master/Issues_img/2021-01-27_12_05_56-Clipboard.png)

Este problema ocurre debido a que nos falta ejecutar el comando `flutter config --enable-web` para habilitar la configuración web.

En caso de que el error persista:
Este problema ocurre al actualizar del canal de **Dev** al canal **Master**.
Una solución para resolver este problema es limpiar el caché de compilación del proyecto (`flutter clean`) y luego ejecutar el proyecto usando el modo de lanzamiento.
Después de eso, puede ejecutarlo en modo de depuración.
