# Implemente una aplicación web Flutter en las páginas de GitLab de forma gratuita

![https://alexandergottlieb.com/assets/images/gitlab-pages-flutter.png](https://alexandergottlieb.com/assets/images/gitlab-pages-flutter.png)

Antes de comenzar, asumiré que ya está almacenando el repositorio de su proyecto Flutter en GitLab. Si está usando GitHub, querrá un tutorial para Git *Hub* Pages, y si no está versionando su código...

Si solo desea ver el código, marque el [repositorio de demostración](https://translate.google.com/website?sl=en&tl=es&u=https://gitlab.com/alexandergottlieb/flutter-web-example).

## Agregar un archivo pequeño

Copie y pegue el código a continuación en un archivo llamado `.gitlab-ci.yml` en la raíz de su proyecto Flutter:

```yaml
image: cirrusci/flutter:latestbefore_script:
  - flutter channel beta
  - flutter upgrade
  - flutter config --enable-web
  - flutter pub getpages:
  stage: deploy
  script:
    - flutter build web
    - cp -r build/web public
  artifacts:
    paths:
      - public
  only:
    - live
```

La estructura de su carpeta debería verse así:

```text
my-flutter-project/
    android
    ios
    lib
    ...
    .gitlab-ci.yml 👍
```

Este archivo le dice a GitLab: cada vez que haya una confirmación en una rama llamada `live`, ejecute `flutter build web`, copie los archivos compilados en una carpeta llamada `public`y haga que esta carpeta esté disponible públicamente en Internet.

Se basa en [una práctica imagen de Docker](https://translate.google.com/website?sl=en&tl=es&u=https://hub.docker.com/r/cirrusci/flutter/) de Cirrus CI que viene con Flutter preinstalado.

> Me gusta mantener mis implementaciones públicas en una rama de git separada. Puede usar cualquier nombre de rama, solo actualice la configuración en **pages>only** en .gitlab-ci.yml.

## Revisa tu base

Puede omitir este paso si desea utilizar un dominio personalizado con su aplicación Flutter en la raíz, por ejemplo `https://midominio.com/`.

De forma predeterminada, las páginas de GitLab publican su aplicación `gitlab.io` en un subdirectorio. Digamos que su nombre de usuario de GitLab es `nombredeusuario` y se llama a su repositorio `nombredelproyecto`, la URL será:

`https://nombredeusuario.gitlab.io/nombredelproyecto/`

Flutter necesita saber que se está ejecutando en `/nombredelproyecto/` en lugar de `/`. Entonces, editemos la [etiqueta base](https://translate.google.com/website?sl=en&tl=es&u=https://developer.mozilla.org/en-US/docs/Web/HTML/Element/base) en `web/index.html`.

Reemplazar:

```html
<base href="/">
```

con:

```html
<base href="/nombredelproyecto/">
```

> ¡Recuerde reemplazar nombredelproyecto con el nombre de su repositorio!

## Desplegar

En esta etapa, ya puede lanzar su aplicación Flutter al mundo. Si su código está listo, cree una nueva rama de git con el nombre `live` y envíe una confirmación.

Comprobando su repositorio en [gitlab.com](https://translate.google.com/website?sl=en&tl=es&u=https://gitlab.com/), en **CI / CD> Trabajos**, debería ver la creación de su aplicación web Flutter.

Mientras se compila, querrá ir a **Configuración> General> Visibilidad...** y asegurarse de que *Pages* esté activado y configurado en *'Todos'* .

![https://alexandergottlieb.com/assets/images/gitlab-pages-visibility.png](https://alexandergottlieb.com/assets/images/gitlab-pages-visibility.png)

De forma predeterminada, esto está configurado en *'Solo miembros del proyecto'* , lo que significa que verá un error de permisos 401 no tan útil cuando cargue el sitio.

Si la compilación fue exitosa, su sitio ahora se ejecutará en algo como `https://nombredeusuario.gitlab.io/nombredelproyecto/`. Puede consultar el enlace exacto en su repositorio en [gitlab.com](https://translate.google.com/website?sl=en&tl=es&u=https://gitlab.com/) en **Configuración> Páginas** .

## Utilice un dominio personalizado

Abra el repositorio de su proyecto Flutter en [gitlab.com](https://translate.google.com/website?sl=en&tl=es&u=https://gitlab.com/) y dirígete a **Configuración> Páginas** , luego presiona 'Nuevo dominio'.

Después de escribir su dominio, debería ver las instrucciones para actualizar sus registros DNS. Necesitará un registro `CNAME` para dirigir realmente el tráfico al sitio, junto con un registro `TXT` para demostrar su propiedad.

> Usar un CNAME es bastante bueno: significa que nunca tendrá que pensar en la dirección IP subyacente donde está alojado su sitio.
