## Algunos requisitos antes de empezar

- Tener instalados los plugins de Flutter y Dart actualizados (en mi caso para este momento tengo la versión 35.3.3 para Flutter y la versión 191.7141.49 para Dart).

- Tener instalados los SDK’s de Flutter (versión 1.5) y Dart (versión 2.3).

- Asegurarse de agregar las rutas de los SDK’s de Flutter y Dart en el PATH del sistema operativo en el que se esté desarrollando.

# Construyendo una aplicación web con Flutter

![https://miro.medium.com/max/800/1*AIQP4KELi9B52sdnsa9iCg.gif](https://miro.medium.com/max/800/1*AIQP4KELi9B52sdnsa9iCg.gif)

A modo de ejemplo construiremos una aplicación similar a Google Keep para crear notas rápidas (temporalmente).

# Configurando el entorno

Para comenzar en Flutter, el primer paso es instalarlo. Flutter está disponible para las tres plataformas más populares: **Windows**, **MacOS** y **Linux**. Flutter es muy fácil de instalar, solo hay que seguir la [guía](https://flutter.dev/docs/get-started/install).

Recursos necesarios:

- ***[Visual Studio Code](https://code.visualstudio.com/Download)***
- ***[Flutter](https://flutter.dev/docs/get-started/install)***

Recursos necesarios para **VSCode:**

- **[Dart](https://marketplace.visualstudio.com/items?itemName=Dart-Code.dart-code)**
- **[Flutter](https://marketplace.visualstudio.com/items?itemName=Dart-Code.flutter)** *(v3.0.1)*

Asegúrate de estar ejecutando la versión ≥ 3.0.1 de la extensión Flutter.

![https://miro.medium.com/max/1000/1*VIipfNjXnJz-iYImgqwA1A.png](https://miro.medium.com/max/1000/1*VIipfNjXnJz-iYImgqwA1A.png)

Una vez descargado e instalado, flutter no está listo para desarrollar para web con la versión estable, así que lo actualizaremos para obtener la última versión que incluye las herramientas necesarias para desarrollar web. Abra la terminal y escriba:

```
flutter upgrade
```

Bien, ahora estamos listos para desarrollar **mobile** y **web**.

# Creando el proyecto

Abra Visual Studio Code, luego Ver -> Paleta de comandos (`Ctrl + Shift + P` o `Command + Shift + P`) y escriba:

```
Flutter: New Web Project
```

![https://miro.medium.com/proxy/1*r5AxF9RL8cOlVp_cW18MuA.png](https://miro.medium.com/proxy/1*r5AxF9RL8cOlVp_cW18MuA.png)

Creé el proyecto con el nombre de “**hello_world**” mismo, puedes dar el nombre que prefieras. Espere mientras Flutter descarga todas las políticas y paquetes necesarios para crear el proyecto, este proceso puede dar un giro por primera vez.

Después de ejecutar el comando, tendrá un proyecto base y lo codificaremos. El proyecto inicial tiene el siguiente esqueleto de archivo:

![https://miro.medium.com/proxy/1*57DMgygZepzDSrNg72Bc3g.png](https://miro.medium.com/proxy/1*57DMgygZepzDSrNg72Bc3g.png)

Utilice el siguiente comando para habilitar el módulo web para flutter

```
flutter packages pub global activate webdev
```

![https://miro.medium.com/proxy/1*wW72kx5vlAOWWsiB8XMG3g.png](https://miro.medium.com/proxy/1*wW72kx5vlAOWWsiB8XMG3g.png)

Finalmente podemos saborear un poco el aleteo en la web, puedes iniciar la aplicación usando **F5** o con el comando:

```
flutter packages pub global run webdev serve
```

![https://miro.medium.com/proxy/1*Yel96OU3Wja5hcLX2kNJog.gif](https://miro.medium.com/proxy/1*Yel96OU3Wja5hcLX2kNJog.gif)

Abra el archivo **main.dart** y elimine todo.
Ahora pega el siguiente código en tu archivo **main.dart**:

```dart
import 'package:flutter_web/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'First Application With Flutter', // title da página
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: Container(
          child: Center(
            child: CounterWidget(),
          ),
        ));
  }
}

class CounterWidget extends StatefulWidget {
  @override
  _CounterWidgetState createState() => _CounterWidgetState();
}

class _CounterWidgetState extends State<CounterWidget> {
  int _counter = 10;

  Future<void> _fimAlert(BuildContext context) {
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Alerta'),
          content: const Text('O contador será reiniciado'),
          actions: <Widget>[
            FlatButton(
              child: Text('Ok'),
              onPressed: () {
                setState(() {
                  _counter = 10;
                });
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            "$_counter",
            style: TextStyle(color: Colors.grey, fontSize: 40.0),
          ),
          FlatButton(
            child: Text('Decrementar'),
            color: Colors.amber,
            onPressed: () {
              setState(() {
                _counter--;
                if (_counter == 0) _fimAlert(context);
              });
            },
          )
        ],
      ),
    );
  }
}
```

Ejecute el código y el resultado será el siguiente:

![https://miro.medium.com/proxy/1*yKxEioT46FznwpjzDXTnaA.gif](https://miro.medium.com/proxy/1*yKxEioT46FznwpjzDXTnaA.gif)

Tenemos una aplicación con control de estados.

# Vayamos a la acción

Ahora cree un nuevo archivo en la carpeta **lib** con el nombre de **todo.dart**:

```dart
import 'package:flutter_web/material.dart';

class TodoPage extends StatefulWidget {
  @override
  _TodoPageState createState() => _TodoPageState();
}

class _TodoPageState extends State<TodoPage> {
  //Controllers são usadas quando precisamos resgatar ou modificar uma propriedade de um widget
  final _textController = TextEditingController();

  //Lista responsável por popular nosso ListView
  List _toDoList = [];

  //Caso o usuário remover uma tarefa, ela ficará em memória nesta variável por um tempo
  Map<String, dynamic> _lastRemoved;

  int _lastRemovedPos;

  void addToDo() {
    setState(() {
      Map<String, dynamic> newTodo = Map();
      newTodo["title"] = _textController.text;
      newTodo["ok"] = false;
      _toDoList.add(newTodo);
      _textController.text = '';
    });
  }

  void _dismissToDo(
      DismissDirection direction, BuildContext context, int index) {
    setState(() {
      _lastRemoved = Map.from(_toDoList[index]);
      _lastRemovedPos = index;
      _toDoList.removeAt(index);

      final snack = SnackBar(
        content: Text('Tarefa \"${_lastRemoved["title"]}\" removida!'),
        action: SnackBarAction(
          label: "Desfazer",
          onPressed: () {
            setState(() {
              _toDoList.insert(_lastRemovedPos, _lastRemoved);
            });
          },
        ),
        duration: Duration(seconds: 5),
      );
      Scaffold.of(context).showSnackBar(snack);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('ToDo Flutter'),
          backgroundColor: Colors.purple,
        ),
        body: Column(
          children: <Widget>[
            Container(
                padding: EdgeInsets.fromLTRB(17.0, 1.0, 7.0, 1.0),
                child: Row(
                  children: <Widget>[
                    Expanded(
                        child: TextField(
                      controller: _textController,
                      decoration: InputDecoration(
                          labelText: 'Nova tarefa',
                          labelStyle: TextStyle(color: Colors.grey)),
                    )),
                    RaisedButton(
                      color: Colors.indigo,
                      child: Text('Adicionar'),
                      textColor: Colors.white,
                      onPressed: addToDo,
                    ),
                  ],
                )),
            Expanded(
                flex: 2,
                child: ListView.builder(
                    padding: EdgeInsets.only(top: 10.0),
                    itemCount: _toDoList.length,
                    itemBuilder: itemBuilder))
          ],
        ));
  }

  Widget itemBuilder(context, index) {
    //Widget responsável por permitir dismiss
    return Dismissible(
      key: Key(DateTime.now().millisecondsSinceEpoch.toString()),
      //A propriedade "background" representa o fundo da nossa tile, o fundo em si não possui ações
      //As ações estão no evento onDismissed
      background: Container(
        color: Colors.redAccent,
        child: Align(
          alignment: Alignment(-0.9, 0.0),
          child: Icon(IconData(0xe900, fontFamily: 'GalleryIcons'), color: Colors.white),
        ),
      ),
      direction: DismissDirection.startToEnd,
      child: CheckboxListTile(
        onChanged: (c) {
          setState(() {
            _toDoList[index]["ok"] = c;
          });
        },
        title: Text(_toDoList[index]["title"]),
        value: _toDoList[index]["ok"],
        secondary: CircleAvatar(
          child: Icon(_toDoList[index]["ok"] ? Icons.check : Icons.error),
        ),
      ),
      onDismissed: (direction) {
        _dismissToDo(direction, context, index);
      },
    );
  }
}
```

Después de cambiar su archivo **main.dart** al siguiente código:

```dart
import 'package:flutter_web/material.dart';
import 'package:hello_world/todo.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Todo with Flutter', // Title da página
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: TodoPage());
  }
}
```

Ahora ejecutemos el código, presione F5 para ver el resultado. Si está siguiendo el tutorial para rascar, el resultado será como en el siguiente gif:

![https://miro.medium.com/proxy/1*ez-h3_9Mwcl2dyc-u-PFtg.gif](https://miro.medium.com/proxy/1*ez-h3_9Mwcl2dyc-u-PFtg.gif)

Tenga en cuenta que nuestro icono es un error y corregiremos este problema ahora. En su vscode, ubique la carpeta web y cree una carpeta llamada **assets:**

![https://miro.medium.com/max/1000/1*RO67vFAdlXMoKM_kLMfITw.png](https://miro.medium.com/max/1000/1*RO67vFAdlXMoKM_kLMfITw.png)

Dentro de la carpeta de activos, cree un archivo llamado **FontManifest.json**.
Una vez creado, pegue el json a continuación en el archivo y ejecute el proyecto nuevamente.

```json
[
    {
      "family": "MaterialIcons",
      "fonts": [
        {
          "asset": "https://fonts.gstatic.com/s/materialicons/v42/flUhRq6tzZclQEJ-Vdg-IuiaDsNcIhQ8tQ.woff2"
        }
      ]
    }
]
```

![https://miro.medium.com/max/800/1*AIQP4KELi9B52sdnsa9iCg.gif](https://miro.medium.com/max/800/1*AIQP4KELi9B52sdnsa9iCg.gif)

# Conclusión

Eso es todo, tenemos una aplicación hecha en Web Flutter totalmente utilizable y rápida de crear.

Como Flutter es un framework orientado al desarrollo móvil, el navegador muestra algunas opciones de Android.

![https://miro.medium.com/proxy/1*DlmXkgc_7_JrkJI711VCNg.png](https://miro.medium.com/proxy/1*DlmXkgc_7_JrkJI711VCNg.png)

Haga doble clic en TextField

**No recomiendo usar Flutter para producción todavía, es mejor esperar hasta que salga la versión estable de Flutter para la versión web.**

El proyecto To Do está disponible en mi [github](https://github.com/fabianosanttana), si solo necesita acceder, [aquí](https://github.com/fabianosanttana/todoflutter).


## Limitaciones

- flutter_web aún no tiene un sistema de complementos. Temporalmente, brindamos acceso a dart: html, dart: js, dart: svg, dart: indexed_db y otras bibliotecas web que le brindan acceso a la gran mayoría de las API del navegador. Sin embargo, se espera que estas bibliotecas sean reemplazadas por una API de complemento diferente.

- No todas las API de Flutter están implementadas en Flutter para web aún.

- El trabajo de rendimiento apenas está comenzando. El código generado por Flutter para web puede ejecutarse lentamente o mostrar un “jank” significativo en la interfaz de usuario.

- En este momento, las interacciones de la interfaz de usuario de escritorio no están completamente completas, por lo que una interfaz de usuario creada con flutter_web puede parecer una aplicación móvil, incluso cuando se ejecuta en un navegador de escritorio.

- El flujo de trabajo de desarrollo solo está diseñado para funcionar con Chrome en este momento.

- Bajo rendimiento: se encuentra en su etapa inicial (infantil), por lo que el rendimiento parece ser bastante bajo.

- No se pueden inspeccionar elementos: esto es en realidad un defecto y una ventaja, como cualquier otra página web, no se pueden inspeccionar los elementos de la aplicación web flutter. Esto es debido a que todo está en lienzo, así que básicamente solo hay un elemento que tiene nuestra aplicación completa.

- El enrutamiento necesita más trabajo: el enrutamiento es una de las características más importantes en cualquier aplicación y si esto no funciona bien, el marco no está ni cerca de estar listo.

### Soporte del navegador

Flutter para web proporciona:

- Un compilador de producción de JavaScript que genera código optimizado y minimizado para la implementación

- Un compilador de desarrollo de JavaScript, que ofrece compilación incremental y hot restart

- Cuando se construyó con el compilador de producción, Flutter admite navegadores basados en Chromium y Safari, tanto en computadoras de escritorio como móviles. También pretendemos ser totalmente compatibles con Firefox y Edge como plataformas específicas, pero nuestra propia cobertura de prueba aún es baja en estos navegadores. Nuestra intención es apoyar la versión actual y las dos últimas publicaciones principales. Se agradece la retroalimentación sobre problemas de rendimiento y rendimiento en todos estos navegadores.

- El soporte de Internet Explorer no está planeado.

- El compilador de desarrollo actualmente solo admite Chrome.
Muestras

Acá unos ejemplos elaborados por Flutter: flutter.github.io/samples.
Vealo en acción aquí: https://jonathanhacklife.gitlab.io/flutter-web/#/

## Recursos:

[Web support for Flutter](https://flutter.dev/web)

[Build and release a web app](https://flutter.dev/docs/deployment/web)

[The Art of Flutter Web](https://medium.com/fluttersg/the-art-of-flutter-flutter-web-383d5db568a0)

[Guía de migración a Flutter Web](https://github.com/flutter/flutter_web/blob/master/docs/migration_guide.md)
